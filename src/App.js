import {Component} from "react";
import './App.css';
import Card from "./Card/Card";

class CardDeck {
    constructor() {
        this.cards = [];
        this.countCards = 52;

        const suits = ['H', 'D', 'C', 'S'];
        const values = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

        suits.forEach((suit, indexSuit) => {
            values.forEach((value, indexValue) => {
                const cards = {
                    suit: suit,
                    ranks: value,
                    id: indexSuit + '-' + indexValue,
                };

                this.cards.push(cards);
            });
        });
    }

    getCard() {
        const randomIndex = Math.floor(Math.random() * this.countCards);
        const [randomCard] = this.cards.splice(randomIndex, 1)
        return randomCard;
    }

    getCards(howMany) {
        const outCards = [];

        for (let i = 0; i < howMany; i++) {
            outCards.push(this.getCard());
            this.countCards -= 1;
        }

        return outCards;
    }
}

class PokerHand {
    constructor(arrayCards) {
        this.array = arrayCards;
    }

    getOutcome() {
        const count = {};
        const findOneOrTwoPair = [];
        const suitCards = [];

        this.array.forEach(card => {
            suitCards.push(card.suit);

            if (count[card.ranks]) {
                return count[card.ranks] += 1;
            }

            count[card.ranks] = 1;
        });

        const flush = suitCards.every(suit => suit === suitCards[0]);
        if (flush) {
            return 'Flush';
        }

        for (const c in count) {
            if (count[c] === 2) {
                findOneOrTwoPair.push(count[c]);
            } else if (count[c] === 3) {
                return 'Three of a kind';
            }
        }

        if (findOneOrTwoPair.length === 2) {
            return 'Two pair';
        } else  if (findOneOrTwoPair.length === 1) {
            return 'One pair';
        }
    }
}

class App extends Component {
    state = {
        selectedCards: new CardDeck().getCards(5)
    };

    pokerHand = () => {
        return new PokerHand(this.state.selectedCards).getOutcome();
    }

    changeCards = () => {
        this.setState({
            selectedCards: new CardDeck().getCards(5)
        });
    }

  render() {
    return (
        <div className="container">
            <p>Hand poker: {this.pokerHand()}</p>
            <div className="block-cards">
                {this.state.selectedCards.map(card => (
                    <Card
                        suit={card.suit}
                        rank={card.ranks}
                        key={card.id}
                    />
                ))}
            </div>
            <button type="button" className="btn" onClick={this.changeCards}>New cards</button>
        </div>
    );
  }
}

export default App;



