import './selfthinker-CSS-Playing-Cards/cards.css';

const classes = {
    'D': 'diams',
    'H': 'hearts',
    'C': 'clubs',
    'S': 'spades'
};

const icons = {
    'D': '♦',
    'H': '♥',
    'C': '♣',
    'S': '♠'
};

const Card = props => {
    return (
        <div className="playingCards [fourColours|faceImages|simpleCards|inText|rotateHand]">
            <div className={"card rank-" + props.rank.toLowerCase() + ' ' + classes[props.suit]}>
                <span className="rank">{props.rank}</span>
                <span className="suit">{icons[props.suit]}</span>
            </div>
        </div>
    );
};

export default Card;


